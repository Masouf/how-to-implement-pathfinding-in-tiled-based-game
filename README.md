# How to implement pathfinding in Tiled-based game

A combination of the Tiled-based game (part 2) and Pathfinding projects (contains Dijkstra's and A* algorithm), based on the KidsCanCode tutorial.

For more details :  
https://www.youtube.com/watch?v=ajR4BZBKTr4&list=PLsk-HSGFjnaGQq7ybM8Lgkh5EMxUWPm2i&index=2

https://www.youtube.com/watch?v=PDPx-z9CwrA&t=682s

## Preview
![](preview.png)
